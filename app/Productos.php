<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Productos extends Model
{
    //
    public function producto()
    {
        return $this->hasMany(Productos::class);
    }
    public function scopeActivos($query)
    {
        return $query->where('marca_baja', 0);
    }
    public function scopeSearch($query, $var)
    {
        //Ver como instanciar el search de la tabla producto
        return $query->where('produc_name', 'like', '%' . $var . '%' );
    }

}
