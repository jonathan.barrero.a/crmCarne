<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Productos;
use File;

class ProductoController extends Controller
{
    public function index()
    {
    	$productos = Productos::activos()->paginate(10);
    	return view('admin.producto.index', compact('productos', $productos));
    }
    public function create()
	{			
		return view('admin.producto.create');
	}	
	public function store(Request $request)
	{
		$messages = [
            'produc_name.required' => 'Es necesario un nombre del producto.',
			'produc_name.min' => 'Es necesario un minimo de 3 caracteres para producto.',
			'produc_code.required' => 'Es necesario un codigo para el producto.',
			'produc_code.unique' => 'El codigo del producto ya esta registrado.',
            'produc_price.required' => 'Es necesario una descripcion para el producto.',                           
            'produc_price.min' => 'No se admiten valores negativos.',
            'produc_price.numeric' => 'El precio solo admite numeros.',
            'produc_stock.required' => 'Es necesario un valor para el producto.',                           
            'produc_stock.min' => 'No se admiten valores negativos.',
            'produc_stock.numeric' => 'El precio solo admite numeros.',
            'produc_stock_min.required' => 'Es necesario un valor para el producto.',                           
            'produc_stock_min.min' => 'No se admiten valores negativos.',
            'produc_stock_min.numeric' => 'El precio solo admite numeros.'
        ];
        $rules = [
			'produc_name' => 'required|min:3',
			'produc_code' => 'required|unique:productos,produc_code',
            'produc_price' => 'required|min:0|numeric', 
            'produc_stock' => 'required|numeric|min:0',
            'produc_stock_min' => 'required|numeric|min:0'
        ];

        $this->validate($request, $rules, $messages);

        $file = $request->file('produc_photo');
        $path = public_path() . '/images/productos';
		$fileName = uniqid() . $file->getClientOriginalName();
		
		$moved = $file->move($path, $fileName);

		if($moved)
		{
			$producto = new Productos();
			$producto->produc_name = $request->input('produc_name');
			$producto->produc_code = $request->input('produc_code');
			$producto->produc_price = $request->input('produc_price');
			$producto->produc_stock = $request->input('produc_stock');
			$producto->produc_stock_min = $request->input('produc_stock_min');		
			//Agregar foto
			$producto->produc_photo = $fileName;
			$producto->marca_baja = 0;

			$producto->save();	
		}
		
		return redirect('/admin/producto');
	}
	public function edit($id)
	{
		$productos = Productos::find($id);
        return view('admin.producto.edit')->with(compact('productos'));  // formulario de registro
	}
	public function update(Request $request, $id)
	{
		// Variables para registro de actualizacion
		return redirect('/admin/producto');
	}
	public function destroy($id)
	{
		$productos = Productos::find($id);
		$fullPath = public_path() . '/images/productos' . $productos->produc_photo;
		$deleted = File::delete($fullPath);
		$productos->delete();
		return back();
	}
	public function delete($id)
	{
		$productos = Productos::find($id);
		$productos->marca_baja = 9;
		$productos->save();
		return back();
	}
}
