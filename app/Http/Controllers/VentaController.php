<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Productos;
use App\Cliente;
use App\Proveedor;
use App\Constants;
use App\Venta;
use App\VentaDetail;

class VentaController extends Controller
{
    public function index()
    {
    	$productos = Productos::activos()->paginate(9);
    	return view('admin.venta.index', compact('productos', $productos));
    }
    public function show($id)
    {
        $productos = Productos::find($id);                
    	return view('admin.venta.show', compact('productos', $productos));
    }
    public function buy()
    {
        $cliente = Cliente::all();
        $proveedor = Proveedor::all();
        $tipoPersona = Constants::tipoPersona()->get();

    	return view('admin.venta.buy')->with(compact('cliente','proveedor','tipoPersona'));
    }
    public function update(Request $request, $id)
    {
        $venta = auth()->user()->venta;
        $venta->status = 'Pendiente';
        $venta->venta_name = $venta->nombre;
        $venta->venta_price = $venta->total;
        $venta->save();
        $stock = VentaDetail::where('venta_id', $id)->get();
        foreach($stocks as $stock)
        {
            $producto = Productos::find($stock->producto_id);
            $producto->produc_stock = $producto->produc_stock - $stock->quantity;
            $producto->save();
        }
        $notification = 'Venta Realizada.';
        return back()->with(compact('notification'));

    }
    public function search(Request $request)
    {
        $var = $request->input('var');
        $productos = Productos::search($var)->paginate(9);
        return view('admin.venta.index', compact('productos', $productos, 'var'));
    }
    public function customer(Request $request, $id)
    {
        $venta = auth()->user()->venta;
        $venta->type_per = $request->type_per;
        $venta->nro_per = $request->nro_per;
        $vemta->save();
        return back();
    }
    public function byTypePer($id)
    {
        if($id == 1)
        {
            $dato = Cliente::where('marca_baja', 0)->get();
        }else{
            $dato = Proveedor::where('marca_baja', 0)->get();
        }
        return $dato;
    }
}
