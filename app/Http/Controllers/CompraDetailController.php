<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\CompraDetail;

class CompraDetailController extends Controller
{
    //
    public function store(Request $request)
    {
    	$compraDetail = new CompraDetail();
    	$compraDetail->compra_id = auth()->user()->compra->id;
    	$compraDetail->producto_id = $request->produc_id;
    	$compraDetail->quantity = $request->quantity;    	
    	$compraDetail->save();
		$notification = 'El producto ah sido agregado a la compra.';		
    	return redirect('/admin/compra')->with(compact('notification'));
    }
    public function destroy(Request $request)
    {
    	$compraDetail = CompraDetail::find($request->compra_detail_id);	    
    	if ($compraDetail->compra_id == auth()->user()->compra->id)
    	{
    		$compraDetail->delete();	
    		$notification = 'El producto ha sido eliminado de la compra.';
    	}	    	    	
    	return back()->with(compact('notification'));
    }
}
