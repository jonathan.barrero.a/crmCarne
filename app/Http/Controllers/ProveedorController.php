<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Proveedor;
use App\Constants;

class ProveedorController extends Controller
{
    public function index()
    {
    	$proveedores = Proveedor::paginate(10);
    	return view('admin.proveedor.index', compact('proveedores', $proveedores));
    }
    public function create()
	{
		$constants = Constants::identificacion()->get();
		$sexo = Constants::sexo()->get();
		$pago = Constants::formaPago()->get();
		$estadoCivil = Constants::estadoCivil()->get();
		return view('admin.proveedor.create')->with(compact('constants','sexo','pago','estadoCivil'));
	}	
	public function store(Request $request)
	{
		$messages = [
            'name_empresa.required' => 'Es necesario un nombre para la empresa.',
            'name_empresa.min' => 'Es necesario un minimo de 4 caracteres para la empresa.',
            'empresa_ident.required' => 'Es necesario una clasificacion para la empresa.',            
            'empres_nit.required' => 'Es necesario llenar el campo Empresa NIT.',   
            'empres_nit.min' => 'Minimo de 1 digitos para celular.',
            'empresa_fec_const.required' => 'Es necesario una fecha de constitucion de la empresa',
            'empresa_direc.required' => 'Es necesario una direccion para la empresa',                       
            'telf_cel.required' => 'Es necesario un nro celular para el cliente.',
            'telf_cel.min' => 'Minimo de 6 digitos para celular.',
            'telf_cel.numeric' => 'Campo de numero de celular solo se admiten numeros.',
            'telf_ofi.required' => 'Es necesario un nro domicilio para el cliente.',
            'telf_ofi.min' => 'Minimo de 6 digitos para domicilio.',            
            'telf_ofi.numeric' => 'Campo de numero de domicilio solo se admiten numeros.'
        ];
        $rules = [
            'name_empresa' => 'required|min:3',            
            'empresa_ident' => 'required',
            'empres_nit' => 'required|min:0',
            'empresa_fec_const' => 'required',
            'empresa_direc' => 'required',            
            'telf_cel' => 'required|min:6|numeric', 
            'telf_ofi' => 'required|min:6|numeric',
        ];

        $this->validate($request, $rules, $messages);

		$proveedor = new Proveedor();		
		$proveedor->name_empresa = $request->input('name_empresa');
		$proveedor->empresa_ident = $request->input('empresa_ident');
		$proveedor->empres_nit = $request->input('empres_nit');
		$proveedor->empresa_fec_const = $request->input('empresa_fec_const');
		$proveedor->empresa_direc = $request->input('empresa_direc');
		$proveedor->telf_cel = $request->input('telf_cel');
		$proveedor->telf_ofi = $request->input('telf_ofi');
		$proveedor->usuario_reg = 'Jonathan';
		$proveedor->marca_baja = 0;
		$proveedor->save();

		return redirect('/admin/proveedor');
	}
	public function edit($id)
	{
		$proveedor = Proveedor::find($id);
        return view('admin.proveedor.edit')->with(compact('proveedor', $proveedor));  // formulario de registro
	}
	public function update(Request $request, $id)
	{
		// Variables para registro de actualizacion
		return redirect('/admin/proveedor');
	}
	public function destroy($id)
	{
		$proveedores = Proveedor::find($id);
		$proveedor->marca_baja = 9;
		$proveedores->save();
		return back();
	}
}
