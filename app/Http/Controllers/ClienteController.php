<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cliente;
use App\Constants;
use App\User;

class ClienteController extends Controller
{
    
    public function index()
    {
    	$clientes = Cliente::paginate(10);    	 
    	return view('admin.cliente.index', compact('clientes', $clientes));
    }
    public function create()
	{
		$constants = Constants::identificacion()->get();
		$sexo = Constants::sexo()->get();
		$pago = Constants::formaPago()->get();
		$estadoCivil = Constants::estadoCivil()->get();
		return view('admin.cliente.create')->with(compact('constants','sexo','pago','estadoCivil'));
	}	
	public function store(Request $request)
	{

		$messages = [
            'name.required' => 'Es necesario un nombre del cliente.',
            'name.min' => 'Es necesario un minimo de 4 caracteres para el cliente.',
            'last_name.required' => 'Es necesario apellido del cliente.',
            'last_name.min' => 'Es necesario un minimo de 4 caracteres para cliente.',
            'client_ident.required' => 'Es necesario un tipo de identificacion.',   
            'client_ci.required' => 'Es necesario un carnet de identidad',
            'client_ci.min' => 'Es necesario un minimo de 7 caracteres identificacion',
            'f_nac.required' => 'Es necesario una fecha de nacimiento',
            'sex.required' => 'Es necesario escoger una opcion "sexo"',            
            'client_esta_civ.required' => 'Es necesario una opcion estado civil.',
            'form_pag.required' => 'Es necesario una opcion de forma de pago.',
            'direc.required' => 'Es necesario una direccion para el cliente.',
            'telf_cel.required' => 'Es necesario un nro celular para el cliente.',
            'telf_cel.min' => 'Minimo de 6 digitos para celular.',
            'telf_cel.numeric' => 'Campo de numero de celular solo se admiten numeros.',
            'telf_dom.required' => 'Es necesario un nro domicilio para el cliente.',
            'telf_dom.min' => 'Minimo de 6 digitos para domicilio.',            
            'telf_dom.numeric' => 'Campo de numero de domicilio solo se admiten numeros.'
        ];
        $rules = [
            'name' => 'required|min:3',
            'last_name' => 'required|min:3',
            'client_ident' => 'required',
            'client_ci' => 'required|min:7',
            'f_nac' => 'required',
            'sex' => 'required',
            'client_esta_civ' => 'required',
            'form_pag' => 'required',
            'direc' => 'required|max:200',
            'telf_cel' => 'required|min:6|numeric', 
            'telf_dom' => 'required|min:6|numeric',
        ];

        $this->validate($request, $rules, $messages);
        

		$cliente = new Cliente();				
		$cliente->name = $request->input('name');
		$cliente->last_name = $request->input('last_name');
		$cliente->client_ident = $request->input('client_ident');
		$cliente->client_ci = $request->input('client_ci');
		$cliente->f_nac = $request->input('f_nac');
		$cliente->sex = $request->input('sex');
		$cliente->client_esta_civ = $request->input('client_esta_civ');
		$cliente->client_prof = $request->input('client_prof');
		$cliente->client_nacio = $request->input('client_nacio');		
		$cliente->form_pag = $request->input('form_pag');
		$cliente->direc = $request->input('direc');		
		$cliente->telf_cel = $request->input('telf_cel');
		$cliente->telf_dom = $request->input('telf_dom');
		$cliente->marca_baja = 0;
		$cliente->usuario_reg = 'Jonathan';  //Auth::user()->name
		$cliente->save();

		return redirect('/admin/cliente');
	}
	public function edit($id)
	{		
		$cliente = Cliente::find($id);
        return view('admin.cliente.edit')->with(compact('cliente', $cliente));  // formulario de registro
	}
	public function update(Request $request, $id)
	{
		// Variables para registro de actualizacion
		return redirect('/admin/cliente');
	}
	public function destroy($id)
	{
		$clientes = Cliente::find($id);
		$clientes->delete();
		return back();
	}
}
