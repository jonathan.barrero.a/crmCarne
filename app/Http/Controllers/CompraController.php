<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Productos;
use App\Cliente;
use App\Proveedor;
use App\Constants;
use App\CompraDetail;
use App\Compra;

class CompraController extends Controller
{
    //
    public function index()
    {
    	$productos = Productos::activos()->paginate(9);
    	return view('admin.compra.index', compact('productos', $productos));
    }
    public function show($id)
    {
        $productos = Productos::find($id);        
    	return view('admin.compra.show', compact('productos', $productos));
    }
    public function buy()
    {
        $cliente = Cliente::all();
        $proveedor = Proveedor::all();
        $tipoPersona = Constants::tipoPersona()->get();

    	return view('admin.compra.buy')->with(compact('cliente','proveedor','tipoPersona'));
    }
    public function update(Request $request, $id)   // se esta pasando un id que no es necesario
    {
        
        $compra = auth()->user()->compra;   
        $compra->status = 'Pendiente';
        $compra->compra_name = $compra->nombre;
        $compra->compra_price = $compra->total;        
        $compra->save(); // update
        $stocks = CompraDetail::where('compra_id', $id)->get();                
        foreach($stocks as $stock)
        {
            $producto = Productos::find($stock->producto_id);            
            $producto->produc_stock = $producto->produc_stock + $stock->quantity;
            $producto->produc_provee = $compra->nombre;
		    $producto->save();    
        }
                
    	$notification = 'Compra Realizada.';
    	return back()->with(compact('notification'));
    }
    public function search(Request $request)
    {
        $var = $request->input('var');
        $productos = Productos::search($var)->paginate(9);
    	return view('admin.compra.index', compact('productos', $productos, 'var'));
    }
    public function customer(Request $request, $id)
    {
        $compra = auth()->user()->compra;
        $compra->type_per = $request->type_per;
        $compra->nro_per = $request->nro_per;
        $compra->save();
        return back();
    }
    public function byTypePer($id)
    {
        if($id == 1)
        {
            $dato = Cliente::where('marca_baja', 0)->get();
            
        }else{
            $dato = Proveedor::where('marca_baja', 0)->get();
        }
        return $dato;
    }
}
