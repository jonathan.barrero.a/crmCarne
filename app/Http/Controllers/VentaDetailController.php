<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\VentaDetail;

class VentaDetailController extends Controller
{
    public function store(Request $request)
    {
    	$ventaDetail = new VentaDetail();
    	$ventaDetail->venta_id = auth()->user()->venta->id;
    	$ventaDetail->producto_id = $request->produc_id;
    	$ventaDetail->quantity = $request->quantity;    	
    	$ventaDetail->save();
		$notification = 'El producto ah sido agregado a la venta.';		
    	return redirect('/admin/venta')->with(compact('notification'));
    }
    public function destroy(Request $request)
    {
    	$ventaDetail = VentaDetail::find($request->venta_detail_id);	    
    	if ($ventaDetail->venta_id == auth()->user()->venta->id)
    	{
    		$ventaDetail->delete();	
    		$notification = 'El producto ha sido eliminado de la venta.';
    	}	    	    	
    	return back()->with(compact('notification'));
    }
}
