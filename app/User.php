<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;



class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function compras()
    {
        return $this->hasMany(Compra::class);
    }
    public function getCompraAttribute()
    {
        
        $compra = $this->compras()->where('status', 'Active')->first();    
        if($compra)
        {            
            return $compra;
        }else{
            $compra = new Compra();
            $compra->status = 'Active';
            $compra->user_id = $this->id;            

            $compra->save();

            return $compra;
        }

    }
    public function ventas()
    {
        return $this->hasMany(Venta::class);
    }
    public function getVentaAttribute()
    {
        $venta = $this->ventas()->where('status', 'Active')->first();
        if($venta)
        {
            return $venta;
        }else{
            $venta = new Venta();
            $venta->status = 'Active';
            $venta->user_id = $this->id;

            $venta->save();
            return $venta;
        }
    }
}
