<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Compra extends Model
{
    public function details()
    {
    	return $this->hasMany(CompraDetail::class);
    }    
    public function getTotalAttribute()
    {
        $total = 0;
        foreach ($this->details as $detail)
        {
            $total += $detail->quantity * $detail->producto->produc_price;
        }
        return $total;
    }
    public function getClienteAttribute()
    {
        $value = $this->type_per;
        
        switch ($value){
            case 1:
                $value = "Cliente";
                break;
            case 2:
                $value = "Proveedor";
                break;
        }

        //if ($value == 1)
        //{
        //    $value = "Cliente";
        //}else{
        //    $value = "Proveedor";
        //}
        
        return $value;
    }
    public function getNombreAttribute()
    {
        $tipo = $this->type_per;
        $value = $this->nro_per;  
              
        if($tipo)
        {
            if($tipo == 1)
            {
                $query = Cliente::where('id',$value)->value('name');        
                
            }else{
                $query = Proveedor::where('id',$value)->value('name_empresa');                
            }
        return $query;
        }                    
    }
    public function getProductAttribute()
    {
        $value = 0;
        $product = $this->details->producto->id;
        if($product)
        {
            return $product;
        }
        return $value;
    }
   
}
