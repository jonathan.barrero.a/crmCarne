<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Venta extends Model
{
    public function details()
    {
    	return $this->hasMany(VentaDetail::class);
    }
}
