<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VentaDetail extends Model
{
    public function producto()
    {
    	return $this->belongsTo(Productos::class);
    }
}
