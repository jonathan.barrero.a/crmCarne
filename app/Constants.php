<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Constants extends Model
{
    public function scopeIdentificacion($query)
    {
    	return $query->where('constant_pref','=', 1)
    				 ->where('constant_corr','>', 0);
    }
    public function scopeSexo($query)
    {
    	return $query->where('constant_pref','=', 2)
    				 ->where('constant_corr','>', 0);
    }
    public function scopeMoneda($query)
    {
    	return $query->where('constant_pref','=', 3)
    				 ->where('constant_corr','>', 0);
    }
    public function scopeEstadoCivil($query)
    {
    	return $query->where('constant_pref','=', 4)
    				 ->where('constant_corr','>', 0);
    }
    public function scopeFormaPago($query)
    {
    	return $query->where('constant_pref','=', 5)
    				 ->where('constant_corr','>', 0);
    }
    public function scopeTipoPersona($query)
    {
    	return $query->where('constant_pref','=', 6)
    				 ->where('constant_corr','>', 0);
    }
}
