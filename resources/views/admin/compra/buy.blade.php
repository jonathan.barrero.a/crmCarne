@extends('layouts.app')

@section('title', 'Listado de Proveedores');

@section('body-class', 'compra-page')

@section('styles')
    <style>
        .team .row .col-md-4 {
            margin-bottom: 5em;
        }
        .row {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            flex-wrap: wrap;
        }
        .row > [class*='col-']{
            display: flex;
            flex-direction: column;
        }
    </style>
@endsection

@section('content')

@include('includes.cabecera')

<div class="main main-raised">
<div class="container">
    

    <div class="section text-center section-landing">
        <h2>Compras seleccionadas</h2>
        
        @if(session('notification'))
            <div class="alert alert-success">
                {{ session('notification') }}            
            </div>            
        @endif
        <div class="panel panel-default">
        <div class="panel-heading">Seleccionar Comprador.</div>
            <div class="panel-body">
                    <form method="post" action="{{ url('/admin/compra/'.auth()->user()->compra->id.'/customer') }}">
                    {{ csrf_field() }}    
                    
                        <div class="row">
                            <div class="col-md-4 form-group">
                                <select name="type_per" class="form-control" id="type_per_id">
                                    <option value="0" selected>Seleccione Tipo</option>
                                    @foreach($tipoPersona as $var_persona)
                                        <option value="{{ $var_persona->constant_corr }}">{{ $var_persona->constant_desc }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-4">
                                <select name="nro_per" class="form-control" id="nroPer_id">
                                    <option value="">Seleccione...</option>                            
                                </select>
                            </div>
                            <button type="submit" class="btn btn-primary btn-sm">
                                <i class="material-icons">done</i> Seleccionar Comprador.
                            </button>
                        </div>              
                    
                    </form>  
                    <form>
                        <div class="form-row">                                            
                                    <div class="col-md-4">                    
                                        <input type="text" class="form-control" value="{{ auth()->user()->compra->cliente }}" disabled>
                                    </div>
                                    <div class="col-md-4">                     
                                        <input type="text" class="form-control" name="name_provee" value="{{ auth()->user()->compra->nombre }}" disabled>
                                    </div>                            
                        </div>
                    </form>   
            </div> 
        </div>

        <hr style="border:10px;">
        
        <div class="panel-body">
            <table class="table">
                <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th class="col-md-2">Nombre</th>                    
                        <th>Stock</th>
                        <th>Stock min.</th>
                        <th>Precio</th>
                        <th>Cantidad</th>
                        <th>SubTotal</th>
                        <th>Acciones</th>
                    </tr>
                </thead>                    
                <tbody>
                    @foreach (auth()->user()->compra->details as $detail)   
                    <tr>
                        <td class="text-center">
                            <img src="{{ url('/images/productos/'.$detail->producto->produc_photo) }}" height="50">   
                        </td>
                        <td class="col-md-2">{{ $detail->producto->produc_name }}</td>                    
                        <td>{{ $detail->producto->produc_stock }}</td>
                        <td>{{ $detail->producto->produc_stock_min }}</td>
                        <td>Bs. {{ $detail->producto->produc_price }}</td>
                        <td>{{ $detail->quantity }}</td>
                        <td>{{ $detail->quantity * $detail->producto->produc_price }}</td>
                        
                        <td class="td-actions">
                            <form method="post" action=" {{ url('/admin/compra/buy'.$detail->producto->id) }} ">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}

                                <input type="hidden" name="compra_detail_id" value="{{ $detail->id }}">

                                <a type="{{ url('/admin/compra/'.$detail->producto->id) }}" rel="tooltip" title="Ver producto" class="btn btn-info btn-simple btn-xs">
                                    <i class="fa fa-info"></i>
                                </a>                            
                                <button type="submit" rel="tooltip" title="Eliminar" class="btn btn-danger btn-simple btn-xs">
                                    <i class="fa fa-times"></i>
                                </button>
                                                    
                            </form>                                    
                        </td>
                    </tr>
                    @endforeach
                </tbody> 
            </table>
            <p><strong>Importe a Pagar: </strong>{{ auth()->user()->compra->total }}</p>        
        </div>

        <div class="text-center">
            @isset(auth()->user()->compra->details)
            <form method="post" action="{{ url('/admin/compra/'.auth()->user()->compra->id) }}">
            {{ csrf_field() }}                
                <button type="submit" class="btn btn-primary btn-round">
                  <i class="material-icons">done</i> Realizar Compra.
                </button>
            </form>
            @endisset
        </div>

    </div>
</div>

</div>

@include('includes.footer')
@endsection

@section('scripts')
    <script src="/js/admin/compra/js.js"></script>
@endsection