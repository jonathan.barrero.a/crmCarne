@extends('layouts.app')

@section('title', 'Crear Cliente')

@section('body-class', 'cliente-page')

@section('content')
<div class="header header-filter" style="background-image: url('https://images.unsplash.com/photo-1423655156442-ccc11daa4e99?crop=entropy&dpr=2&fit=crop&fm=jpg&h=750&ixjsv=2.1.0&ixlib=rb-0.3.5&q=50&w=1450');">

</div>

<div class="main main-raised">
<div class="container">

    <div class="section">
        <h2 class="title text-center">Registrar nuevo cliente</h2>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form method="post" action=" {{ url('/admin/cliente') }} ">
            {{ csrf_field() }}

            <div class="row">
            <div class="col-sm-6">
                <div class="form-group label-floating">
                    <label class="control-label">Nombre</label>
                    <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                </div> 
            </div>
            <div class="col-sm-6">
                <div class="form-group label-floating">
                    <label class="control-label">Apellido</label>
                    <input type="text" class="form-control" name="last_name" value="{{ old('last_name') }}">
                </div>
            </div>            
            </div>
            <div class="row">
            <div class="col-sm-6">  
                <div class="form-group label-floating">              
                    <select name="client_ident" class="form-control">
                    <option disabled selected>Elige Identificacion</option>
                    @foreach($constants as $constant)
                        @if(old('client_ident') == $constant->constant_corr)
                        <option value="{{ $constant->constant_corr }}" selected>{{ $constant->constant_desc }}</option>
                        @else
                        <option value="{{ $constant->constant_corr }}">{{ $constant->constant_desc }}</option>
                        @endif
                    @endforeach
                    </select>
                </div>  
            </div>
            <div class="col-sm-6">
                <div class="form-group label-floating">
                    <label class="control-label">Nro. Identificacion</label>
                    <input type="text" class="form-control" name="client_ci" value="{{ old('client_ci') }}">
                </div>
            </div>            
            </div>
            <div class="row">
            <div class="col-sm-6">
                <div class="form-group label-floating">
                    <label class="control-label">Fecha de nacimiento</label>
                    <input type="text" class="form-control datetimepicker" name="f_nac" value="{{ old('f_nac') }}">
                </div> 
            </div>
            <div class="col-sm-6">
                <div class="form-group label-floating">
                    <select name="sex" class="form-control">
                      <option disabled selected>Sexo</option>
                      @foreach($sexo as $var_sex)
                        @if(old('sex') == $var_sex->constant_corr)
                            <option value="{{ $var_sex->constant_corr }}" selected>{{ $var_sex->constant_desc }}</option>
                        @else
                            <option value="{{ $var_sex->constant_corr }}">{{ $var_sex->constant_desc }}</option>
                        @endif
                      @endforeach
                    </select>
                </div>
            </div>            
            </div>
            <div class="row">
            <div class="col-sm-6">
                <div class="form-group label-floating">
                    <select name="client_esta_civ" class="form-control">
                      <option disabled selected>Estado Civil</option>
                      @foreach($estadoCivil as $var_civil)
                        @if(old('client_esta_civ') == $var_civil->constant_corr)
                            <option value="{{ $var_civil->constant_corr }}" selected>{{ $var_civil->constant_desc }}</option>
                        @else
                            <option value="{{ $var_civil->constant_corr }}">{{ $var_civil->constant_desc }}</option>
                        @endif
                      @endforeach
                    </select>
                </div>
            </div> 
            <div class="col-sm-6">
                <div class="form-group label-floating">
                    <label class="control-label">Profesion</label>
                    <input type="text" class="form-control" name="client_prof" value="{{ old('client_prof') }}">
                </div>
            </div>            
            </div>
            <div class="row">
            <div class="col-sm-6">
                <div class="form-group label-floating">
                    <label class="control-label">Nacionalidad</label>
                    <input type="text" class="form-control" name="client_nacio" value="{{ old('client_nacio') }}">
                </div> 
            </div>                            
            </div>
            <div class="row">
            <div class="col-sm-6">
                <div class="form-group label-floating">
                    <select name="form_pag" class="form-control">
                      <option disabled selected>Forma de Pago</option>
                      @foreach($pago as $var_pago)
                        @if(old('form_pag') == $var_pago->constant_corr)
                            <option value="{{ $var_pago->constant_corr }}" selected>{{ $var_pago->constant_desc }}</option>
                        @else 
                            <option value="{{ $var_pago->constant_corr }}">{{ $var_pago->constant_desc }}</option>
                        @endif
                      @endforeach
                    </select>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group label-floating">
                    <label class="control-label">Direccion</label>
                    <input type="text" class="form-control" name="direc" value="{{ old('direc') }}">
                </div>
            </div>            
            </div>
            <div class="row">
            <div class="col-sm-6">
                <div class="form-group label-floating">
                    <label class="control-label">Telefono Celular</label>
                    <input type="text" class="form-control" name="telf_cel" value="{{ old('telf_cel') }}">
                </div> 
            </div>
            <div class="col-sm-6">
                <div class="form-group label-floating">
                    <label class="control-label">Telefono Fijo</label>
                    <input type="number" class="form-control" name="telf_dom" value="{{ old('telf_dom') }}">
                </div>
            </div>            
            </div>
                        
            <div class="row text-center">                 
                <button class="btn btn-primary">Registrar Cliente</button>
                <a href="{{ url('/admin/cliente') }}" class="btn btn-default">Volver</a>
            </div>
        </form>

    </div>

</div>

</div>

@include('includes.footer')
@endsection

@section('scripts')
    <script src="{{ asset('/js/admin/cliente/cliente.js') }}"></script>
@endsection