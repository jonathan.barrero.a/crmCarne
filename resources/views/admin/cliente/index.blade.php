@extends('layouts.app')

@section('title', 'Listado de Clientes');

@section('body-class', 'client-page')

@section('content')

@include('includes.cabecera')

<div class="main main-raised">
<div class="container">
    <div class="section text-center section-landing">
        <h2>Listado de Clientes</h2>
        <div class="team">
            <div class="row">
                <a href=" {{ url('admin/cliente/create') }} " class="btn btn-primary btn-round">Agregar nuevo Cliente</a>
                <table class="table">
                    <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th class="col-md-2">Nombre</th>
                            <th class="col-md-2">Apellido</th>
                            <th class="col-md-2">Telefono</th>
                            <th class="col-md-2">Direccion</th>
                            <th class="col-md-2">Estado</th>
                            <th class="col-md-2">Acciones</th>
                        </tr>
                    </thead>                    
                    <tbody>
                        @foreach($clientes as $cliente)   
                        <tr>
                            <td class="text-center">{{ $cliente->id }}</td>
                            <td class="col-md-2">{{ $cliente->name }}</td>
                            <td class="col-md-2">{{ $cliente->last_name }}</td>
                            <td class="col-md-2">{{ $cliente->telf }}</td>
                            <td class="col-md-2">{{ $cliente->direc }}</td>
                            @if($cliente->marca_baja == 0 )
                                <td class="col-md-2"><span class="badge badge-pill badge-success">Activo</span></td>
                            @else
                                <td class="col-md-2"><span class="badge badge-pill badge-danger">Inactivo</span></td>
                            @endif
                            <td class="td-actions text-right">
                                <form method="post" action=" {{ url('/admin/cliente/'.$cliente->id.'/delete') }} ">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}

                                    <a type="#" rel="tooltip" title="Ver Cliente" class="btn btn-info btn-simple btn-xs">
                                        <i class="fa fa-user"></i>
                                    </a>
                                    <a href=" {{url('/admin/cliente/'.$cliente->id.'/edit') }} " rel="tooltip" title="Editar Cliente" class="btn btn-success btn-simple btn-xs">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <button type="submit" rel="tooltip" title="Eliminar" class="btn btn-danger btn-simple btn-xs">
                                        <i class="fa fa-times"></i>
                                    </button>
                                                        
                                </form>                                    
                            </td>
                        </tr>
                        @endforeach
                    </tbody> 
                </table>

                {{ $clientes->links() }}
            </div>
        </div>
    </div>
</div>

</div>

@include('includes.footer')
@endsection

