@extends('layouts.app')

@section('title', 'Listado de Proveedores');

@section('body-class', 'client-page')

@section('content')

@include('includes.cabecera')

<div class="main main-raised">
<div class="container">
    <div class="section text-center section-landing">
        <h2>Listado de Proveedores</h2>
        <div class="team">
            <div class="row">
                <a href=" {{ url('admin/proveedor/create') }} " class="btn btn-primary btn-round">Agregar nuevo Proveedor</a>
                <table class="table">
                    <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th class="col-md-2">Empresa</th>
                            <th class="col-md-2">NIT</th>
                            <th class="col-md-2">Telefono</th>
                            <th class="col-md-2">Direccion</th>
                            <th class="col-md-2">Estado</th>
                            <th class="col-md-2">Acciones</th>
                        </tr>
                    </thead>                    
                    <tbody>
                        @foreach($proveedores as $proveedor)   
                        <tr>
                            <td class="text-center">{{ $proveedor->id }}</td>
                            <td class="col-md-2">{{ $proveedor->name_empresa }}</td>
                            <td class="col-md-2">{{ $proveedor->empres_nit }}</td>
                            <td class="col-md-2">{{ $proveedor->telf_cel }}</td>
                            <td class="col-md-2">{{ $proveedor->empresa_direc }}</td>
                            @if($proveedor->marca_baja == 0)
                                <td class="col-md-2"><span class="badge badge-pill badge-success">Activo</span></td>
                            @else
                                <td class="col-md-2"><span class="badge badge-pill badge-danger">Inactivo</span></td>
                            @endif                            
                            <td class="td-actions text-right">
                                <form method="post" action=" {{ url('/admin/proveedor/'.$proveedor->id) }} ">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}

                                    <a type="#" rel="tooltip" title="Ver Proveedores" class="btn btn-info btn-simple btn-xs">
                                        <i class="fa fa-user"></i>
                                    </a>
                                    <a href=" {{url('/admin/proveedor/'.$proveedor->id.'/edit') }} " rel="tooltip" title="Editar proveedor" class="btn btn-success btn-simple btn-xs">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <button type="submit" rel="tooltip" title="Eliminar" class="btn btn-danger btn-simple btn-xs">
                                        <i class="fa fa-times"></i>
                                    </button>
                                                        
                                </form>                                    
                            </td>
                        </tr>
                        @endforeach
                    </tbody> 
                </table>

                {{ $proveedores->links() }}
            </div>
        </div>
    </div>
</div>

</div>

@include('includes.footer')
@endsection

