@extends('layouts.app')

@section('title', 'Crear Cliente')

@section('body-class', 'cliente-page')

@section('content')

@include('includes.cabecera')

<div class="main main-raised">
<div class="container">

    <div class="section">
        <h2 class="title text-center">Registrar nuevo provvedor</h2>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form method="post" action=" {{ url('/admin/proveedor'.$proveedor->id.'/edit) }} ">
            {{ csrf_field() }}

            <div class="row">
            <div class="col-sm-6">
                <div class="form-group label-floating">
                    <label class="control-label">Empresa</label>
                    <input type="text" class="form-control" name="name_empresa" value="{{ old('name_empresa', $proveedor->name_empresa) }}">
                </div> 
            </div>
            

            <div class="row text-center">                 
                <button class="btn btn-primary">Registrar Proveedor</button>
                <a href="{{ url('/admin/proveedor') }}" class="btn btn-default">Volver</a>
            </div>

        </form>

    </div>

</div>

</div>

@include('includes.footer')
@endsection

@section('scripts')
    <script src="/js/admin/proveedor/proveedor.js"></script>
@endsection