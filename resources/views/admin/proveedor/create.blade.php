@extends('layouts.app')

@section('title', 'Crear Cliente')

@section('body-class', 'cliente-page')

@section('content')

@include('includes.cabecera')

<div class="main main-raised">
<div class="container">

    <div class="section">
        <h2 class="title text-center">Registrar nuevo provvedor</h2>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form method="post" action=" {{ url('/admin/proveedor') }} ">
            {{ csrf_field() }}

            <div class="row">
            <div class="col-sm-6">
                <div class="form-group label-floating">
                    <label class="control-label">Empresa</label>
                    <input type="text" class="form-control" name="name_empresa" value="{{ old('name_empresa') }}">
                </div> 
            </div>
            <div class="col-sm-6">                
                <select name="empresa_ident" class="form-control">
                  <option disabled selected>Elige Identificacion</option>
                  @foreach($constants as $constant)
                    @if(old('client_ident') == $constant->constant_corr)
                      <option value="{{ $constant->constant_corr }}" selected>{{ $constant->constant_desc }}</option>
                    @else
                      <option value="{{ $constant->constant_corr }}">{{ $constant->constant_desc }}</option>
                    @endif
                  @endforeach
                </select>
            </div>            
            </div>
            <div class="row">
            <div class="col-sm-6">
                <div class="form-group label-floating">
                    <label class="control-label">Empresa NIT</label>
                    <input type="text" class="form-control" name="empres_nit" value="{{ old('empres_nit') }}">
                </div>
            </div>            
            <div class="col-sm-6">
                <div class="form-group label-floating">
                    <label class="control-label">Fecha de nacimiento</label>
                    <input type="text" class="form-control datetimepicker" name="empresa_fec_const" value="{{ old('empresa_fec_const') }}">
                </div> 
            </div>
            </div>
            <div class="row">
            <div class="col-sm-6">
                <div class="form-group label-floating">
                    <label class="control-label">Direccion</label>
                    <input type="text" class="form-control" name="empresa_direc" value="{{ old('empresa_direc') }}">
                </div>
            </div>            
            <div class="col-sm-3">
                <div class="form-group label-floating">
                    <label class="control-label">Telefono Celular</label>
                    <input type="number" class="form-control" name="telf_cel" value="{{ old('telf_cel') }}">
                </div> 
            </div>
            <div class="col-sm-3">
                <div class="form-group label-floating">
                    <label class="control-label">Telefono Oficina</label>
                    <input type="number" class="form-control" name="telf_ofi" value="{{ old('telf_ofi') }}">
                </div> 
            </div>
            </div>

            <div class="row text-center">                 
                <button class="btn btn-primary">Registrar Proveedor</button>
                <a href="{{ url('/admin/proveedor') }}" class="btn btn-default">Volver</a>
            </div>

        </form>

    </div>

</div>

</div>

@include('includes.footer')
@endsection

@section('scripts')
    <script src="/js/admin/proveedor/proveedor.js"></script>
@endsection