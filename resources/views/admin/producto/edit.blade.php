@extends('layouts.app')

@section('title', 'Actualizar producto')

@section('body-class', 'product-page')

@section('content')

@include('includes.cabecera')

<div class="main main-raised">
<div class="container">

    <div class="section">
        <h2 class="title text-center">Actualizar producto</h2>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form method="post" action=" {{ url('/admin/producto/'.$var_producto->id.'/edit) }} ">
            {{ csrf_field() }}

            <div class="row">
            <div class="col-sm-6">
                <div class="form-group label-floating">
                    <label class="control-label">Nombre del producto</label>
                    <input type="text" class="form-control" name="produc_name" value="{{ old('produc_name'), $var_producto->produc_name }}">
                </div> 
            </div>
            <div class="col-sm-6">
                <div class="form-group label-floating">
                    <label class="control-label">Precio</label>
                    <input type="number" step="0.01" class="form-control" name="produc_price" value="{{ old('produc_price'), $var_producto->produc_price }}">
                </div>
            </div>            
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group label-floating">
                        <label class="control-label">Stock</label>
                        <input type="text" class="form-control" name="produc_stock" value="{{ old('produc_stock'), $var_producto->produc_stock }}">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group label-floating">
                        <label class="control-label">Stock Minimo</label>
                        <input type="text" class="form-control" name="produc_stock_min" value="{{ old('produc_stock_min'), $var_producto->produc_stock_min }}">
                    </div>
                </div>
            </div>
        
            <button class="btn btn-primary">Guardar Producto</button>
            <a href="{{ url('/admin/producto') }}" class="btn btn-default">Volver</a>

        </form>

    </div>

</div>

</div>

@include('includes.footer')
@endsection

@section('scripts')
    <script src="/js/admin/proveedor/proveedor.js"></script>
@endsection