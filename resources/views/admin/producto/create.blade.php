@extends('layouts.app')

@section('title', 'Crear Producto')

@section('body-class', 'cliente-page')

@section('content')

@include('includes.cabecera')

<div class="main main-raised">
<div class="container">

    <div class="section">
        <h2 class="title text-center">Registrar nuevo Producto</h2>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form method="post" action=" {{ url('/admin/producto') }} " enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="row">
            <div class="col-sm-6">
                <div class="form-group label-floating">
                    <label class="control-label">Nombre Producto</label>
                    <input type="text" class="form-control" name="produc_name" value="{{ old('produc_name') }}">
                </div> 
            </div>
            <div class="col-sm-6">
                <div class="form-group label-floating">
                    <label class="control-label">Codigo Producto</label>
                    <input type="text" class="form-control" name="produc_code" value="{{ old('produc_code') }}">
                </div> 
            </div>
            <div class="col-sm-6">                
                <div class="form-group label-floating">
                    <label class="control-label">Precio Bs.</label>
                    <input type="text" class="form-control" name="produc_price" value="{{ old('produc_price') }}">
                </div> 
            </div>            
            </div>
            <div class="row">
            <div class="col-sm-6">
                <div class="form-group label-floating">
                    <label class="control-label">Stock 00.00</label>
                    <input type="text" class="form-control" name="produc_stock" value="{{ old('produc_stock') }}">
                </div>
            </div>            
            <div class="col-sm-6">
                <div class="form-group label-floating">
                    <label class="control-label">Stock Minimo 00.00</label>
                    <input type="text" class="form-control" name="produc_stock_min" value="{{ old('produc_stock_min') }}">
                </div> 
            </div>
            </div>            
            <div class="col-sm-6">
                <div class="form-group label-floating">                    
                    <input type="file" name="produc_photo" required>
                    <button type="submit" class="btn btn-primary btn-round">Subir Imagen</button>                    
                </div> 
            </div>
            </div>

            <div class="row text-center">                 
                <button class="btn btn-primary">Registrar Productos</button>
                <a href="{{ url('/admin/producto') }}" class="btn btn-default">Volver</a>
            </div>

        </form>

    </div>

</div>

</div>

@include('includes.footer')
@endsection

@section('scripts')
    
@endsection