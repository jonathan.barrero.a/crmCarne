@extends('layouts.app')

@section('title', 'Listado de Proveedores');

@section('body-class', 'client-page')

@section('content')

@include('includes.cabecera')

<div class="main main-raised">
<div class="container">
    <div class="section text-center section-landing">
        <h2>Listado de Productos</h2>
        <div class="team">
            <div class="row">
                <a href=" {{ url('admin/producto/create') }} " class="btn btn-primary btn-round">Agregar nuevo Producto</a>
                <table class="table">
                    <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th class="col-md-2">Nombre</th>
                            <th class="col-md-2">Codigo</th>
                            <th class="col-md-1">Precio</th>
                            <th class="col-md-1">Stock</th>
                            <th class="col-md-1">Stock min.</th>
                            <th class="col-md-2">Estado</th>
                            <th class="col-md-2">Acciones</th>
                        </tr>
                    </thead>                    
                    <tbody>
                        @foreach($productos as $var_producto)   
                        <tr>
                            <td class="text-center">{{ $var_producto->id }}</td>
                            <td class="col-md-2">{{ $var_producto->produc_name }}</td>
                            <td class="col-md-2">{{ $var_producto->produc_code }}</td>
                            <td class="col-md-1">{{ $var_producto->produc_price }}</td>
                            <td class="col-md-1">{{ $var_producto->produc_stock }}</td>
                            <td class="col-md-1">{{ $var_producto->produc_stock_min }}</td>
                            @if ($var_producto->marca_baja == 0)
                                <td class="col-md-2"><span class="badge badge-pill badge-success">Disponible</span></td>
                            @else
                                <td class="col-md-2"><span class="badge badge-pill badge-danger">De baja</span></td>
                            @endif                            
                            <td class="td-actions text-right">
                                <form method="post" action=" {{ url('/admin/producto/'.$var_producto->id) }} ">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}

                                    <a type="#" rel="tooltip" title="Ver Productos" class="btn btn-info btn-simple btn-xs">
                                        <i class="fa fa-user"></i>
                                    </a>
                                    <a href=" {{url('/admin/producto/'.$var_producto->id.'/edit') }} " rel="tooltip" title="Editar producto" class="btn btn-success btn-simple btn-xs">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <button type="submit" rel="tooltip" title="Eliminar" class="btn btn-danger btn-simple btn-xs">
                                        <i class="fa fa-times"></i>
                                    </button>
                                                        
                                </form>                                    
                            </td>
                        </tr>
                        @endforeach
                    </tbody> 
                </table>

                {{ $productos->links() }}
            </div>
        </div>
    </div>
</div>

</div>

@include('includes.footer')
@endsection

