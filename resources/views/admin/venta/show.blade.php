@extends('layouts.app')

@section('title', 'Actualizar producto')

@section('body-class', 'profile-page')

@section('content')

		<div class="header header-filter" style="background-image: url('/img/examples/city.jpg');"></div>

		<div class="main main-raised">
			<div class="profile-content">
	            <div class="container">
	                <div class="row">
	                    <div class="profile">
	                        <div class="avatar">
	                            <img src="{{ url('/images/productos/'.$productos->produc_photo) }}" alt="Circle Image" class="img-circle img-responsive img-raised">
	                        </div>
	                        <div class="name">
	                            <h3 class="title">{{$productos->produc_name}}</h3>
																<table class="table">
																	<thead>
																		<tr>
																				<th class="col-md-2 text-center">Detalle</th>
																				<th class="col-md-2 text-center">Datos</th>
																				
																		</tr>
																</thead>      
																<tbody>
																	<tr>
																		<td class="col-md-2 text-center">Codigo Producto</td>
																		<td class="col-md-2 text-center">{{$productos->produc_code}}</td>																		
																	</tr>
																	<tr>
																		<td class="col-md-2 text-center">Precio Producto</td>
																		<td class="col-md-2 text-center">{{$productos->produc_price}}</td>																		
																	</tr>
																	<tr>
																		<td class="col-md-2 text-center">Stock</td>
																		<td class="col-md-2 text-center">{{$productos->produc_stock}}</td>																		
																		@if ($productos->produc_stock <= $productos->produc_stock_min)
																			<span class="badge badge-pill badge-danger">Alerta Producto minimo!.</span>
																		@endif
																	</tr>
																	<tr>
																		<td class="col-md-2 text-center">Stock Minimo</td>
																		<td class="col-md-2 text-center">{{$productos->produc_stock_min}}</td>																		
																	</tr>
																	<tr>
																		<td class="col-md-2 text-center">Proveedor</td>
																		<td class="col-md-2 text-center">{{$productos->provee ? $productos->provee : "No hay proveedor"}}</td>																		
																	</tr>
																</tbody>
															</table>	
	                        </div>
	                    </div>
	                </div>
	                <div class="description text-center">
                        
	                </div>

					<div class="row">
						<div class="col-md-6 col-md-offset-3">
							<div class="profile-tabs">
			                    <div class="nav-align-center">									
			                    	<div class="text-center">
															<a href="{{ url('/admin/venta') }}" class="btn btn-default btn-round">Volver</a>
			                    		<button class="btn btn-primary btn-round" data-toggle="modal" data-target="#modalAddCart">
			                    			<i class="material-icons">add</i> Agregar a la Venta.
			                    		</button>									
			                    	</div>
				                    
								</div>
							</div>
							<!-- End Profile Tabs -->
						</div>
	                </div>

	            </div>
	        </div>
		</div>


<!-- Modal -->
<div class="modal fade" id="modalAddCart" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Seleccione cantidad Producto para la Venta</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="{{ url('/admin/venta') }}">
      	{{ csrf_field() }}
      		<input type="hidden" name="produc_id" value="{{ $productos->id }}">
	      <div class="modal-body">
	        <input type="number" name="quantity" value="1" step="0.0001" class="form-control">
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
	        <button type="submit" class="btn btn-primary">Agregar</button>
	      </div>
      </form>      
    </div>
  </div>
</div>

@include('includes.footer')
@endsection


