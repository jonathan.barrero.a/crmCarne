@extends('layouts.app')

@section('title', 'Listado de Proveedores');

@section('body-class', 'client-page')

@section('styles')
    <style>
        .team .row .col-md-4 {
            margin-bottom: 5em;
        }
        .row {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            flex-wrap: wrap;
        }
        .row > [class*='col-']{
            display: flex;
            flex-direction: column;
        }
    </style>
@endsection

@section('content')

@include('includes.cabecera')

<div class="main main-raised">
<div class="container">
    <div class="section text-center section-landing">
        @if(session('notification'))
            <div class="alert alert-success">
                {{ session('notification') }}            
            </div>            
        @endif
        <h2>Listado de Productos Venta.</h2>
        <div class="panel-body">
            <form action="{{ url('admin/venta/search') }}" class="navbar-form text-center form-inline" method="GET">
                <div class="form-group">
                    <input type="text" class="form-control" name="var" placeholder="Buscar." value="{{ isset($var) ? $var : '' }}">
                </div>
                <button class="btn btn-default">Buscar</button>
            </form>
        </div>
        <div class="team">
            <div class="row">
                @foreach($productos as $producto)
                    <div class="col-md-4">
                        <div class="team-player">
                            <img src="{{ url('/images/productos/'.$producto->produc_photo) }}" alt="Thumbnail Image" class="img-raised img-circle">
                            <h4 class="title">{{ $producto->produc_name }}</h4>
                            <a href="{{ url('/admin/venta/'.$producto->id) }}" class="btn btn-primary">Ver</a>
                        </div>
                    </div>
                @endforeach
            </div>
        {{ $productos->appends(['var'])->links() }}
        </div>
    </div>
</div>

</div>

@include('includes.footer')
@endsection

