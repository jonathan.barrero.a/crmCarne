@extends('layouts.app')

@section('title', 'Dashboard')

@section('body-class', 'cliente-page')

@section('content')
<div class="header header-filter" style="background-image: url('/img/home/santa_cruz2.jpg');">

</div>

<div class="main main-raised">
<div class="container">

    <div class="section">
        <h2 class="title text-center">Dashboard</h2>

        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <div class="col-md-12">
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>Compra</h3>
                    </div>
                    <div class="icon">
                     <i class="fa fa-cart-arrow-down"></i>
                    </div>
                    <a href="{{ url('/admin/compra') }}" class="small-box-footer">
                        Ir... <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>Venta</h3>
                    </div>
                    <div class="icon">
                     <i class="fa fa-cart-plus"></i>
                    </div>
                    <a href="{{ url('/admin/venta') }}" class="small-box-footer">
                        Ir... <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>Cliente</h3>
                    </div>
                    <div class="icon">
                     <i class="fa fa-address-card"></i>
                    </div>
                    <a href="{{ url('/admin/cliente') }}" class="small-box-footer">
                        Ir... <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3>Proveedor</h3>
                    </div>
                    <div class="icon">
                     <i class="fa fa-address-card"></i>
                    </div>
                    <a href="{{ url('/admin/proveedor') }}" class="small-box-footer">
                        Ir... <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-purple">
                    <div class="inner">
                        <h3>Productos</h3>
                    </div>
                    <div class="icon">
                     <i class="fa fa-dropbox"></i>
                    </div>
                    <a href="{{ url('/admin/producto') }}" class="small-box-footer">
                        Ir... <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-yellowone">
                    <div class="inner">
                        <h3>Reporte</h3>
                    </div>
                    <div class="icon">
                     <i class="fa fa-bar-chart"></i>
                    </div>
                    <a href="#" class="small-box-footer">
                        Ir... <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>

</div>

</div>

@include('includes.footer')
@endsection

