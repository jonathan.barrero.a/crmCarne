@extends('layouts.app')

@section('body-class', 'landing-page')

@section('content')
<div class="header header-filter" style="background-image: url('/img/home/home.jpg');">
<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            AQUI LOGO
        </div>
    </div>
</div>
</div>

<div class="main main-raised">
<div class="container">
    <div class="section text-center section-landing">
        <div class="row">
            
        </div>

        <div class="features">
            <div class="row">
                <div class="col-md-4">
                    <div class="info">
                        <div class="icon icon-primary">
                            <img src="{{ url('/img/home/filete.png') }}" alt="Thumbnail Image" class="img-raised img-circle">
                        </div>
                        <h4 class="info-title">Calidad</h4>
                        <p>Nuestro principal objetivo es asegurar la máxima calidad y seguridad alimentaria en todos nuestros productos cárnicos.</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="info">
                        <div class="icon icon-success">
                            <img src="{{ url('/img/home/movil.png') }}" alt="Thumbnail Image" class="img-raised img-circle">
                        </div>
                        <h4 class="info-title">Venta Online</h4>
                        <p>Hacemos que comprar sea más fácil.</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="info">
                        <div class="icon icon-danger">
                        <img src="{{ url('/img/home/mapa.png') }}" alt="Thumbnail Image" class="img-raised img-circle">
                        </div>
                        <h4 class="info-title">Cercania</h4>
                        <p>Siete carnicerías a su disposición en la ciudad de Santa Cruz.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section text-center">
        <h2 class="title">Here is our team</h2>

        <div class="team">
            <div class="row">
                <div class="col-md-4">
                    <div class="team-player">
                        <img src="../assets/img/avatar.jpg" alt="Thumbnail Image" class="img-raised img-circle">
                        <h4 class="title">Gigi Hadid <br />
                            <small class="text-muted">Model</small>
                        </h4>
                        <p class="description">You can write here details about one of your team members. You can give more details about what they do. Feel free to add some <a href="#">links</a> for people to be able to follow them outside the site.</p>
                        <a href="#pablo" class="btn btn-simple btn-just-icon"><i class="fa fa-twitter"></i></a>
                        <a href="#pablo" class="btn btn-simple btn-just-icon"><i class="fa fa-instagram"></i></a>
                        <a href="#pablo" class="btn btn-simple btn-just-icon btn-default"><i class="fa fa-facebook-square"></i></a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="team-player">
                        <img src="../assets/img/christian.jpg" alt="Thumbnail Image" class="img-raised img-circle">
                        <h4 class="title">Christian Louboutin<br />
                            <small class="text-muted">Designer</small>
                        </h4>
                        <p class="description">You can write here details about one of your team members. You can give more details about what they do. Feel free to add some <a href="#">links</a> for people to be able to follow them outside the site.</p>
                        <a href="#pablo" class="btn btn-simple btn-just-icon"><i class="fa fa-twitter"></i></a>
                        <a href="#pablo" class="btn btn-simple btn-just-icon"><i class="fa fa-linkedin"></i></a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="team-player">
                        <img src="../assets/img/kendall.jpg" alt="Thumbnail Image" class="img-raised img-circle">
                        <h4 class="title">Kendall Jenner<br />
                            <small class="text-muted">Model</small>
                        </h4>
                        <p>You can write here details about one of your team members. You can give more details about what they do. Feel free to add some <a href="#">links</a> for people to be able to follow them outside the site.</p>
                        <a href="#pablo" class="btn btn-simple btn-just-icon"><i class="fa fa-google-plus"></i></a>
                        <a href="#pablo" class="btn btn-simple btn-just-icon"><i class="fa fa-youtube-play"></i></a>
                        <a href="#pablo" class="btn btn-simple btn-just-icon btn-default"><i class="fa fa-twitter"></i></a>
                    </div>
                </div>
            </div>
        </div>

    </div>


    <div class="section landing-section">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h2 class="text-center title">Work with us</h2>
                <h4 class="text-center description">Divide details about your product or agency work into parts. Write a few lines about each one and contact us about any further collaboration. We will responde get back to you in a couple of hours.</h4>
                <form class="contact-form">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group label-floating">
                                <label class="control-label">Your Name</label>
                                <input type="email" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group label-floating">
                                <label class="control-label">Your Email</label>
                                <input type="email" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="form-group label-floating">
                        <label class="control-label">Your Messge</label>
                        <textarea class="form-control" rows="4"></textarea>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-md-offset-4 text-center">
                            <button class="btn btn-primary btn-raised">
                                Send Message
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>

</div>

@include('includes.footer')
@endsection