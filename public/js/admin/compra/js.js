$(function(){
    $('#type_per_id').on('change', onSelectProjectChange);
});


    function onSelectProjectChange() {
        var typePer_id = $(this).val();
        //ajax
        if(!typePer_id)
        {
            $('#nroPer_id').html('<option value="">Seleccione..</option>');
            return;
        }else if(typePer_id == 1){
            $.get('/api/admin/compra/'+typePer_id+'/type', function (data){
                var html_select = '<option value="">Seleccione Tipo</option>';
                for(var i=0; i<data.length;i++)
                    html_select += '<option value="'+data[i].id+'">'+data[i].name+'</option>';
                $('#nroPer_id').html(html_select);
            });
        }else{
            $.get('/api/admin/compra/'+typePer_id+'/type', function (data){
                var html_select = '<option value="">Seleccione..</option>';
                for(var i=0; i<data.length;i++)
                    html_select += '<option value="'+data[i].id+'">'+data[i].name_empresa+'</option>';
                $('#nroPer_id').html(html_select);
            });
        }
    }
