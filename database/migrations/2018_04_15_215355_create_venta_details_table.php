<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVentaDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('venta_details', function (Blueprint $table) {
            $table->increments('id');

            //FK con compra
            $table->integer('venta_id')->unsigned();
            $table->foreign('venta_id')->references('id')->on('compras');
 
            // FK con producto
            $table->integer('producto_id')->unsigned();
            $table->foreign('producto_id')->references('id')->on('productos');
 
            $table->float('quantity',8,4);
            $table->integer('discount')->default(0);   //descuento por %
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('venta_details');
    }
}
