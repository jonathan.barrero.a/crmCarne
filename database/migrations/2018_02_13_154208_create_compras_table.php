<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComprasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('compras', function (Blueprint $table) {
            $table->increments('id');
            $table->date('order_date')->nullable();
            $table->date('arrived_date')->nullable();
            $table->integer('type_per')->nullable();
            $table->integer('nro_per')->nullable(); //codigo del cliente cargado.
            $table->string('status');      //Activo, Pendiente, Aprobado, Cancelado, Finalizado.
            $table->string('compra_name')->nullable();
            $table->double('compra_price',15,2)->nullable(); //  Precio de compra hecha
            //FK user
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('compras');
    }
}
