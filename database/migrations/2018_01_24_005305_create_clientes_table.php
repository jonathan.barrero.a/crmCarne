<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->increments('id');                                     
            $table->string('name');
            $table->string('last_name');            
            $table->smallInteger('client_ident');
            $table->string('client_ci');
            $table->date('f_nac');
            $table->smallInteger('sex');
            $table->smallInteger('client_esta_civ'); 
            $table->string('client_prof')->nullable();
            $table->string('client_nacio')->nullable();
            $table->integer('form_pag');
            $table->string('direc')->nullable();
            $table->integer('telf_cel');
            $table->integer('telf_dom');
                                                                                    
            $table->smallInteger('marca_baja');
            $table->string('usuario_reg');
            $table->string('usuario_modi')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
}
