<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('produc_name');
            $table->string('produc_code');
            $table->float('produc_price',15,4);
            $table->float('produc_stock',8,4);
            $table->float('produc_stock_min',8,4);
            $table->string('produc_photo');
            $table->string('produc_provee')->nullable();
            $table->smallInteger('marca_baja');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
