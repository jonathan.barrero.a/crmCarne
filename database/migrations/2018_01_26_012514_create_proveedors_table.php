<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProveedorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proveedors', function (Blueprint $table) {
            $table->increments('id');            
            $table->string('name_empresa');
            $table->smallInteger('empresa_ident');
            $table->string('empres_nit');
            $table->date('empresa_fec_const');
            $table->string('empresa_direc');
            $table->integer('telf_cel');
            $table->integer('telf_ofi');

            $table->smallInteger('marca_baja');
            $table->string('usuario_reg');
            $table->string('usuario_modi')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proveedors');
    }
}
