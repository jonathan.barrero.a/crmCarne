<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//------------------------- clientes------------------------------------
Route::get('/admin/cliente', 'ClienteController@index'); //home index cliente
Route::get('/admin/cliente/create', 'ClienteController@create'); //crear formulario
Route::post('/admin/cliente', 'ClienteController@store'); //registro del producto
Route::get('/admin/cliente/{id}/edit', 'ClienteController@edit'); //crear formulario
Route::post('/admin/cliente/{id}/edit', 'ClienteController@update'); //registro del producto
Route::delete('/admin/cliente/{id}', 'ClienteController@destroy'); // form eliminar
//-----------------------------------------------------------------------

//------------------------- proveedores------------------------------------
Route::get('/admin/proveedor', 'ProveedorController@index'); //home index proveedor
Route::get('/admin/proveedor/create', 'ProveedorController@create'); //crear formulario
Route::post('/admin/proveedor', 'ProveedorController@store'); //registro del producto
Route::get('/admin/proveedor/{id}/edit', 'ProveedorController@edit'); //crear formulario
Route::post('/admin/proveedor/{id}/edit', 'ProveedorController@update'); //registro del producto
Route::delete('/admin/proveedor/{id}', 'ProveedorController@destroy'); // form eliminar
//-----------------------------------------------------------------------

//------------------------- Productos------------------------------------
Route::get('/admin/producto', 'ProductoController@index'); //home index productos
Route::get('/admin/producto/create', 'ProductoController@create'); //crear formulario
Route::post('/admin/producto', 'ProductoController@store'); //registro del producto
Route::get('/admin/producto/{id}/edit', 'ProductoController@edit'); //crear formulario
Route::post('/admin/producto/{id}/edit', 'ProductoController@update'); //registro del producto
//Route::delete('/admin/producto/{id}', 'ProductoController@destroy'); // form eliminar
Route::delete('/admin/producto/{id}', 'ProductoController@delete'); // form dar de baja el producto asociado
//-----------------------------------------------------------------------


//-------------------------Compra------------------------------------
Route::get('/admin/compra', 'CompraController@index'); //home index Compras
Route::get('/admin/compra/search', 'CompraController@search'); //busqueda de productos de compra
Route::get('/admin/compra/create', 'CompraController@create'); //crear formulario
Route::get('/admin/compra/buy', 'CompraController@buy'); //formulario de la compra del producto detalles
Route::post('/admin/compra/buy', 'CompraDetailController@destroy'); // Eliminacion del producto del carrito de compra
Route::get('/admin/compra/{id}', 'CompraController@show'); //Mostrar productos para compra
Route::post('/admin/compra', 'CompraDetailController@store'); //registro del producto compra para detalle compra 
Route::get('/admin/compra/{id}/edit', 'CompraController@edit'); //crear formulario
Route::post('/admin/compra/{id}', 'CompraController@update'); //registro del producto
Route::post('/admin/compra/{id}/customer', 'CompraController@customer'); //registro del comprador cliente o proveedor
Route::delete('/admin/compra/{id}', 'CompraDetailController@destroy'); // form eliminar
//------------------
Route::post('/admin/compra/{id}', 'CompraController@update');  // routa para realizar el pedido de la compra
//-----------------------------------------------------------------------


//-------------------------Venta--------------------------------------
Route::get('/admin/venta', 'VentaController@index'); //home index venta
Route::get('/admin/venta/create', 'VentaController@create'); //crear formulario
Route::post('/admin/venta', 'VentaController@store'); //registro del producto
Route::get('/admin/venta/{id}/edit', 'VentaController@edit'); //crear formulario
Route::post('/admin/venta/{id}/edit', 'VentaController@update'); //registro del producto
Route::post('/admin/venta', 'VentaDetailController@store'); //Registro del producto venta para el detalle venta.
Route::get('/admin/venta/buy', 'VentaController@buy'); //formulario de la compra del producto detalles
Route::get('/admin/venta/{id}', 'VentaController@show'); //Mostrar productos para la venta.
Route::post('/admin/venta/buy', 'VentaDetailController@destroy'); // Eliminacion del producto del carrito de compra
Route::post('/admin/venta/{id}/customer', 'VentaController@customer');
Route::delete('/admin/venta/{id}', 'VentaDetailController@destroy');
//---------------------------